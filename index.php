<?php if (substr_count($_SERVER[‘HTTP_ACCEPT_ENCODING’], ‘gzip’)) ob_start(“ob_gzhandler”); else ob_start(); ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
	<!-- Required meta tags always come first -->
	<meta name="author" content="Alex,Adam,Isaac,Nick" />
	<meta name="description" content="" />
	<meta name="keywords" content="New Zealand Barber Boss Cuts" />
	<meta name="Resource-type" content="Document" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Boss Cuts</title>

	<!-- Css   -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="../css/jquery.fullpage.css" />
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../css/blueimp-gallery.min.css">
	<!-- Scripts   -->
	<script src="../jquery/jquery-2.2.4.min.js"></script>
	<script src="../js/tether.min.js"></script>
	<script src="../jquery/jquery.fullpage.js"></script>
	<script src="../js/bootstrap.js"></script>

	<!-- ONESCROLL   -->
	<script type="text/javascript">
		$(document).ready(function() {
			$('#fullpage').fullpage({
				//Navigation
				 fixedElements: '#header, #Footer',
				anchors: ['Cover', 'About', 'Services', 'Opening', 'Gallery', 'Contact'],
				//Scrolling
				css3: true,
				scrollingSpeed: 800,
				//Accessibility
				keyboardScrolling: true,
				animateAnchor: true,
				recordHistory: true,
				//Scrolling
				css3: true,
				scrollingSpeed: 700,
				fitToSection: true,
				fitToSectionDelay: 1000,
				scrollBar: false,
				easing: 'easeInOutCubic',
				easingcss3: 'ease',
				loopBottom: true,
				loopTop: true,
				loopHorizontal: true,
				continuousVertical: false,
				scrollOverflow: false,
        scrollOverflowOptions: null,
				touchSensitivity: 15,
				normalScrollElementTouchThreshold: 5,
			})
		});
	</script>
	<!-- Login   -->
	<script type="text/javascript">
		$(document).ready(function() {
			$('#login-trigger').click(function() {
				$(this).next('#login-content').slideToggle();
				$(this).toggleClass('active');

				if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
				else $(this).find('span').html('&#x25BC;')
			})
		});
	</script>


</head>

<!-- Information   -->

<body>
	<!--  Header  -->
	<?php require 'Elements/Header.php';?>
		<!--  Footer  -->
		<?php require 'Elements/Footer.php';?>
	<div id="fullpage">
		<!-- Cover Photo   -->
		<div data-menuanchor="Cover" class="section active">
			<div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">

				<div class="carousel-inner" role="listbox">
					<div class="carousel-item active">
						<img src="../Images/Cover/Cover1.jpg" alt="First slide" style="height:75em;" class="coverimage hidden-md-down">
						<img src="../Images/Cover/Cover1.jpg" alt="First slide" style="height:62em;" class="coverimage hidden-lg-up">
					</div>
					<div class="carousel-item">
						<img src="../Images/Cover/Cover2.jpg" alt="First slide" class="hidden-md-down">
						<img src="../Images/Cover/Cover2.jpg" alt="First slide" style="height:62em;" class="coverimage hidden-lg-up">
					</div>
					<div class="carousel-item">
						<img src="../Images/Cover/Cover3.jpg" alt="First slide" class="hidden-md-down">
						<img src="../Images/Cover/Cover3.jpg" alt="First slide" style="height:62em;" class="coverimage hidden-lg-up">
					</div>
					<div class="carousel-item">
						<img src="../Images/Cover/Cover4.jpg" alt="First slide" class="hidden-md-down">
						<img src="../Images/Cover/Cover4.jpg" alt="First slide" style="height:62em;" class="coverimage hidden-lg-up">
					</div>
				</div>
				<a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
					<i class="fa fa-arrow-left arrow fa-2x" aria-hidden="true"></i>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#carousel" role="button" data-slide="next">
					<i class="fa fa-arrow-right arrow fa-2x" aria-hidden="true"></i>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>

		<!-- About   -->
		<div data-menuanchor="About" class="section">
			<?php require 'Sites/About.php';?>
		</div>

		<!-- Services   -->
		<div data-menuanchor="Services" class="section">
			<?php require 'Sites/Services.php';?>
		</div>

		<!-- Hours  -->
		<div data-menuanchor="Opening" class="section">
			<?php require 'Sites/Opening.php';?>
		</div>


		<!-- Gallery   -->
		<div data-menuanchor="Gallery" class="section">
			<?php require 'Sites/Gallery.php';?>
		</div>

		<!--  Contact  -->
		<div data-menuanchor="Contact" class="section">
			<?php require 'Sites/Testomonial.php';?>
		</div>
	</div>

	<!-- Gallery -->
	<script src="js/blueimp-gallery.min.js"></script>
	
	<script>
		document.getElementById('links').onclick = function(event) {
			event = event || window.event;
			var target = event.target || event.srcElement,
				link = target.src ? target.parentNode : target,
				options = {
					index: link,
					event: event
				},
				links = this.getElementsByTagName('a');
			blueimp.Gallery(links, options);
		};
	</script>

	<div id="blueimp-gallery" class="blueimp-gallery">
		<div class="slides"></div>
		<h3 class="title"></h3>
		<a class="prev">‹</a>
		<a class="next">›</a>
		<a class="close">×</a>
		<a class="play-pause"></a>
		<ol class="indicator"></ol>
	</div>

	<?php require 'Sites/modals.php';?>

</body>
</html>