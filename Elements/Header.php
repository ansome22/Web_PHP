<Header id="Header">
  <!--social-->
  <nav class="navbar fixed-nav-bar grey-background">
    <!--Button for mobile to see header links-->
    <button class="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar2">
    <i class="fa fa-bars" aria-hidden="true"></i>  
  </button>
<!--Contact Information-->
    <div class="row nav-inline top hidden-md-down">
      <ul class="ul-no-style ul-padding col-xs-12 col-sm-8 col-md-10">
        <li class="list-inline">
          <a class="nav-link active" href="https://goo.gl/maps/WKP9PyKWJc72"><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i>Address: 174 Clarkin Road, Hamilton, New Zealand 3214</a>
          <a class="nav-link" href="tel:07-855-6874"><i class="fa fa-phone fa-lg" aria-hidden="true"></i> Phone: 07-855-6874</a>
          <a class="nav-link" href="mailto:Bosscutsnz@gmail.com"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i> Email: Bosscutsnz@gmail.com</a>
        </li>
      </ul>
      <!--social links-->
      <ul class="ul-no-style header-social col-xs-4 col-md-2">
        <li class="list-inline">
          <a href="https://www.facebook.com/Boss-Cuts-Barber-Shop-742987982431729/" class="space facebook">
            <i class="fa fa-facebook-square fa-fg grow" aria-hidden="true"></i>
          </a>
        </li>
        <li class="list-inline">
          <a href="https://www.instagram.com/Boss-Cuts-Barber-Shop" class="space instagram">
            <i class="fa fa-instagram fa-fg grow" aria-hidden="true"></i>
          </a>
        </li>
        <li class="list-inline">
          <a href="https://twitter.com/Boss-Cuts-Barber-Shop" class="space twitter">
            <i class="fa fa-twitter-square fa-fg grow" aria-hidden="true"></i>
          </a>
        </li>
      </ul>
    </div>



    <!--Login  Mobile-->
    <ul class="nav navbar-nav pull-xs-right header-links hidden-lg-up">
      <li class="nav-item">
        <a class="nav-link orange-text" data-toggle="modal" data-target="#Login">Login</a>
      </li>

      <li class="nav-item">
        <a href="../Sites/Sign-up.php" class="nav-link orange-text">Signup</a>
      </li>
    </ul>

    <div class="collapse navbar-toggleable-md" id="exCollapsingNavbar2">
      <a class="navbar-brand hidden-md-down" href="#">
        <img class="logo responsive-img" src="../Images/Logo.png" alt title="Logo">
      </a>

      <ul class="nav navbar-nav">
        <li class="nav-item active">
          <a href="../index.php" class="nav-link orange-text">Home |</a>
        </li>
        <li class="nav-item">
          <a href="../index.php#About" class="nav-link orange-text">About |</a>
        </li>
        <li class="nav-item">
          <a href="../index.php#Services" class="nav-link orange-text">Services |</a>
        </li>
        <li class="nav-item">
          <a href="../index.php#Opening" class="nav-link orange-text">Times |</a>
        </li>
        <li class="nav-item">
          <a href="../index.php#Gallery" class="nav-link orange-text">Gallery |</a>
        </li>
        <li class="nav-item">
          <a href="../index.php#Contact" class="nav-link orange-text">Testomonials |</a>
        </li>
      </ul>

    </div>
    <!--Login  Desktop-->
    <ul class="nav navbar-nav pull-xs-right header-links hidden-md-down">
      <li id="login" class="list-inline">
        <a class="orange-text" id="login-trigger" href="#">Log in <span>▼</span></a>
        <div id="login-content" class="login-trigger pull-xs-right">
          <form id="login" action="Sites/logon.php" method="POST" onSubmit="action">
            <fieldset id="inputs">
              <div class="input-group margin-bottom-sm">
                <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                <input class="form-control" name = "username" type="text" placeholder="Username">
              </div>
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                <input class="form-control" name = "password" type="password" placeholder="Password">
              </div>
            </fieldset>
            <fieldset id="actions">
              <input type="submit" id="submit" value="Log in">
              <input type="checkbox" checked="checked">
              <small class="text-muted">Keep me signed in</small>
            </fieldset>
            <input type="hidden" name="tablename" value="login">
          </form>
        </div>
      </li>
      <li id="signup" class="list-inline"><a href="../Sites/Sign-Up.php" class="navButton orange-text">Sign up</a></li>
    </ul>
  </nav>
</Header>