<head>
	<title>Boss Cuts</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="author" content="Alex,Adam,Isaac,Nick" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="Resource-type" content="Document" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Css   -->
	<link rel="stylesheet" type="text/css" href="../css/jquery.fullPage.css" />
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../css/simplelightbox.min.css">
	<link rel="stylesheet" href="../css/bootstrap.css">

	<!-- Scripts   -->
	<script src="../jquery/jquery-2.2.3.min.js"></script>
	<script src="../jquery/jquery.fullPage.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/simple-lightbox.min.js"></script>
	<!-- Gallery   -->
	<script type="text/javascript">
		$(function() {
			var $gallery = $('.gallery a').simpleLightbox();
		});
	</script>

	<!-- ONESCROLL   -->
	<script type="text/javascript">
		$(document).ready(function() {
			$('#fullpage').fullpage({
				//Navigation
				anchors: ['Cover', 'About', 'Services', 'Opening', 'Gallery', 'Contact', 'Temp'],
				//Scrolling
				css3: true,
				scrollingSpeed: 800,
				loopBottom: true,
				//Accessibility
				keyboardScrolling: true,
			})
		});
	</script>
	<!-- Login   -->
	<script type="text/javascript">
		$(document).ready(function() {
			$('#login-trigger').click(function() {
				$(this).next('#login-content').slideToggle();
				$(this).toggleClass('active');

				if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
				else $(this).find('span').html('&#x25BC;')
			})
		});
	</script>
</head>