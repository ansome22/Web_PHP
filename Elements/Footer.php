
<footer id="Footer" class="footer">
  <!--will contain the social media-->
    <div class="hidden-md-down">
    <ul style="padding: 0 40px;" class="nav nav-inline">
      <li class="nav-item"><a class="orange-text" href="#Index">Home</a></li>
      <li class="nav-item"><a class="orange-text" data-toggle="modal" data-target="#Bookingmodal">Booking</a></li>
      <li class="nav-item"><a class="orange-text" data-toggle="modal" data-target="#ModalFeedback">Feedback</a></li>
      <li class="nav-item"><a class="orange-text" data-toggle="modal" data-target="#ModalQuery">Query</a></li>
      <li class="nav-item"><a class="orange-text" href="../Sites/licencing.php">License</a></li>
    </ul>
    </div>
    
    <div class="hidden-lg-up">
    <ul style="font-size: 2.1em;text-align: center;" class="nav nav-inline">
      <li class="nav-item"><a class="orange-text" title="Home" href="#Index"><i class="fa fa-home" aria-hidden="true"></i></a></li>
      <li class="nav-item"><a class="orange-text" title="Login" data-toggle="modal" data-target="#Login"><i class="fa fa-sign-in" aria-hidden="true"></i></a></li>
      <li class="nav-item"><a class="orange-text" title="Booking" data-toggle="modal" data-target="#Bookingmodal"><i class="fa fa-book" aria-hidden="true"></i></a></li>
      <li class="nav-item"><a class="orange-text" data-toggle="modal" data-target="#ModalFeedback"><i class="fa fa-commenting-o" aria-hidden="true"></i></a></li>
      <li class="nav-item"><a class="orange-text" data-toggle="modal" data-target="#ModalQuery"><i class="fa fa-comments-o" aria-hidden="true"></i></a></li>
      <li class="nav-item"><a class="orange-text" title="Licencing" href="../Sites/licencing.php"><i class="fa fa-file-text" aria-hidden="true"></i></a></li>
    </ul>
  </div> 
</footer>
