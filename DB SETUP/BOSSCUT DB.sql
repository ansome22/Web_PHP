/*
-- Boss Cuts DDL script.
-- Created By Nick Robinson.
--Created at 30th May 2016.
*/
/**/
--

-- Basic Table Construction.

--
CREATE TABLE `aboutus` (
  `aboutID` int(10) NOT NULL,
  `aboutText` varchar(60) NOT NULL,
  `visible` char(1) NOT NULL,
  `order` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `addresses` (
  `addressNO` int(10) NOT NULL,
  `suburb` varchar(60) NOT NULL,
  `city` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `bookingdetails` (
  `bookingDetailNO` int(10) NOT NULL,
  `bookingNO` int(10) NOT NULL,
  `serviceNO` int(10) NOT NULL,
  `staffNO` int(10) NOT NULL,
  `productCode` int(10) NOT NULL,
  `quantity` int(10) NOT NULL,
  `discount` decimal(5,0) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `bookinghead` (
  `bookingNO` int(10) NOT NULL,
  `clientNO` int(10) NOT NULL,
  `bookingDate` date NOT NULL,
  `bookingTime` time NOT NULL,
  `endTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `certifications` (
  `certificationCode` int(10) NOT NULL,
  `certificationName` varchar(60) DEFAULT NULL,
  `certificationDesc` varchar(60) DEFAULT NULL,
  `statusCode` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `clients` (
  `clientNO` int(10) NOT NULL,
  `lastName` varchar(60) DEFAULT NULL,
  `firstName` varchar(60) DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `joinedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phone` int(15) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `streetAddress` varchar(60) DEFAULT NULL,
  `suburb` varchar(60) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `postCode` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `companycontact` (
  `branchNO` int(10) NOT NULL,
  `branchName` varchar(60) NOT NULL,
  `Facebook` varchar(60) NOT NULL,
  `aboutUs` varchar(60) NOT NULL,
  `foundedDate` date NOT NULL,
  `email` varchar(60) NOT NULL,
  `phone` int(7) NOT NULL,
  `streetAddress` varchar(60) NOT NULL,
  `suburb` varchar(60) NOT NULL,
  `city` varchar(60) NOT NULL,
  `status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `message` (
  `messageNO` int(10) NOT NULL,
  `messageTypeCode` varchar(10) NOT NULL,
  `staffNO` int(10),
  `clientNO` int(10),
  `fullName` varchar(60),
  `email` varchar(60),
  `phone` int(7),
  `messageDate` date,
  `subject` varchar(60),
  `messageDescription` varchar(60),
  `starRating` int(1),
  `serviceReceived` varchar(60),
  `replyDescription` varchar(60),
  `replyDate` date,
  `replyStaff` int(10)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `messagetypes` (
  `messageTypeCode` varchar(10) NOT NULL,
  `typeDescription` varchar(60) NOT NULL,
  `status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `news` (
  `newsID` int(10) NOT NULL,
  `newsDate` date NOT NULL,
  `subject` varchar(60) NOT NULL,
  `searchWords` varchar(60) NOT NULL,
  `newsText` varchar(1000) NOT NULL,
  `visible` char(1) NOT NULL,
  `expireDate` date ,
  `order` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `operationhours` (
  `branchNO` int(10) NOT NULL,
  `day` varchar(10) NOT NULL,
  `startTime` time NOT NULL,
  `finishTime` time NOT NULL,
  `status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `prebookingdetails` (
  `preBookingNO` int(10) NOT NULL,
  `clientNO` int(10) NOT NULL,
  `serviceNO` int(10) NOT NULL,
  `preferredStylists` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `prebookinghead` (
  `preBookingNO` int(10) NOT NULL,
  `clientNO` int(10) NOT NULL,
  `preferredDate` date DEFAULT NULL,
  `preferredTime` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `products` (
  `productCode` int(10) NOT NULL,
  `productName` varchar(60) NOT NULL,
  `productDesc` varchar(60) NOT NULL,
  `cost` decimal(10,0) NOT NULL,
  `unitPrice` decimal(10,0) NOT NULL,
  `qoh` varchar(5) NOT NULL,
  `imageLink` varchar(60) NOT NULL,
  `statusCode` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `qualifications` (
  `qualificationCode` int(10) NOT NULL COMMENT 'Qualification Code',
  `qualificationName` varchar(60) NOT NULL COMMENT 'Qualification Name',
  `qualificationDesc` varchar(60) NOT NULL COMMENT 'Qualification Description',
  `statusCode` char(1) NOT NULL COMMENT 'Qualification Status Code'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `services` (
  `serviceNO` int(10) NOT NULL,
  `serviceName` varchar(60) NOT NULL,
  `serviceDesc` varchar(60) NOT NULL,
  `costLowRange` decimal(6,0) NOT NULL,
  `costHighRange` decimal(6,0) NOT NULL,
  `timeRequired` int(4) NOT NULL,
  `statusCode` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `specialistskills` (
  `serviceNO` int(10) NOT NULL,
  `staffNO` int(10) NOT NULL,
  `dateObtained` date NOT NULL,
  `statusCode` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `staffcertications` (
  `certificationCode` int(10) NOT NULL,
  `staffNO` int(10) NOT NULL,
  `dateObtained` date NOT NULL,
  `statusCode` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `staffqualifications` (
  `qualificationCode` int(10) NOT NULL,
  `staffNO` int(10) NOT NULL,
  `dateObtained` date NOT NULL,
  `statusCode` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `stylists` (
  `staffNO` int(10) NOT NULL,
  `lastName` varchar(60) DEFAULT NULL,
  `firstName` varchar(60) DEFAULT NULL,
  `knownName` varchar(60) DEFAULT NULL,
  `yearsExperience` int(15) DEFAULT NULL,
  `aboutMe` varchar(500) DEFAULT NULL,
  `joinedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(60) DEFAULT NULL,
  `phone` int(15) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `homeTown` varchar(60) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `streetAddress` varchar(60) DEFAULT NULL,
  `suburb` varchar(60) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `postCode` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `userlogon` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) DEFAULT NULL,
  `userTypeCode` char(1) DEFAULT NULL,
  `clientNO` int(10) DEFAULT NULL,
  `staffNO` int(10) DEFAULT NULL,
  `statusCode` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `usertypes` (
  `userTypeCode` char(1) NOT NULL,
  `typeDesc` varchar(60) DEFAULT NULL,
  `statusCode` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `status` (
  `statusCode` char(1) NOT NULL,
  `statusDescription` varchar(60)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--

-- Indexing for Constraints And Primary Keys. :)

--
ALTER TABLE `aboutus`
  ADD PRIMARY KEY (`aboutID`);

ALTER TABLE `addresses`
  ADD PRIMARY KEY (`addressNO`);

ALTER TABLE `bookingdetails`
  ADD PRIMARY KEY (`bookingDetailNO`),
  ADD KEY `bookingNO` (`bookingNO`,`serviceNO`,`productCode`),
  ADD KEY `serviceNO` (`serviceNO`),
  ADD KEY `productCode` (`productCode`),
  ADD KEY `staffNO` (`staffNO`);

ALTER TABLE `bookinghead`
  ADD PRIMARY KEY (`bookingNO`),
  ADD KEY `clientNO` (`clientNO`);


ALTER TABLE `certifications`
  ADD PRIMARY KEY (`certificationCode`);

ALTER TABLE `clients`
  ADD PRIMARY KEY (`clientNO`);

ALTER TABLE `companycontact`
  ADD PRIMARY KEY (`branchNO`);

ALTER TABLE `message`
  ADD PRIMARY KEY (`messageNO`),
  ADD KEY `clientNO` (`clientNO`),
  ADD KEY `staffNO` (`staffNO`),
  ADD KEY `messageTypeCode` (`messageTypeCode`);

ALTER TABLE `messagetypes`
  ADD PRIMARY KEY (`messageTypeCode`);

ALTER TABLE `news`
  ADD PRIMARY KEY (`newsID`);

ALTER TABLE `operationhours`
  ADD PRIMARY KEY (`branchNO`);

ALTER TABLE `prebookingdetails`
  ADD PRIMARY KEY (`preBookingNO`),
  ADD KEY `preBookingNO` (`preBookingNO`,`clientNO`,`serviceNO`,`preferredStylists`),
  ADD KEY `clientNO` (`clientNO`),
  ADD KEY `serviceNO` (`serviceNO`),
  ADD KEY `preferredStylists` (`preferredStylists`);

ALTER TABLE `prebookinghead`
  ADD PRIMARY KEY (`preBookingNO`),
  ADD KEY `clientNO` (`clientNO`),
  ADD KEY `preBookingNO` (`preBookingNO`);

ALTER TABLE `products`
  ADD PRIMARY KEY (`productCode`);

ALTER TABLE `qualifications`
  ADD PRIMARY KEY (`qualificationCode`);

ALTER TABLE `services`
  ADD PRIMARY KEY (`serviceNO`);

ALTER TABLE `specialistskills`
  ADD PRIMARY KEY (`staffNO`),
  ADD KEY `serviceNO` (`serviceNO`);


ALTER TABLE `staffcertications`
  ADD PRIMARY KEY (`certificationCode`,`staffNO`),
  ADD KEY `certStaffFK_invalid` (`staffNO`);


ALTER TABLE `staffqualifications`
  ADD PRIMARY KEY (`qualificationCode`,`staffNO`),
  ADD KEY `staffqualifications_ibfk_1` (`staffNO`);


ALTER TABLE `stylists`
  ADD PRIMARY KEY (`staffNO`);


ALTER TABLE `userlogon`
  ADD PRIMARY KEY (`username`),
  ADD KEY `logonUserTypesFK_invalid` (`userTypeCode`),
  ADD KEY `logonCustomerFK_invalid` (`clientNO`),
  ADD KEY `staffNO` (`staffNO`);


ALTER TABLE `usertypes`
  ADD PRIMARY KEY (`userTypeCode`);


ALTER TABLE `status`
  ADD PRIMARY KEY (`statusCode`);
--

-- Modifies "I prefer this way"

--

ALTER TABLE `aboutus`
  MODIFY `aboutID` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bookingdetails`
  MODIFY `bookingDetailNO` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bookinghead`
  MODIFY `bookingNO` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `certifications`
  MODIFY `certificationCode` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `clients`
  MODIFY `clientNO` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `companycontact`
  MODIFY `branchNO` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `news`
  MODIFY `newsID` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `prebookinghead`
  MODIFY `preBookingNO` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `services`
  MODIFY `serviceNO` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `qualifications`
  MODIFY `qualificationCode` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `stylists`
  MODIFY `staffNO` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `products`
  MODIFY `productCode` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `addresses`
  MODIFY `addressNO` int(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE `message`
  MODIFY `messageNO` int(10) NOT NULL AUTO_INCREMENT;
--

-- Constraints.

--
ALTER TABLE `bookingdetails`
  ADD CONSTRAINT `bookingdetails_ibfk_1` FOREIGN KEY (`bookingNO`) REFERENCES `bookinghead` (`bookingNO`),
  ADD CONSTRAINT `bookingdetails_ibfk_2` FOREIGN KEY (`serviceNO`) REFERENCES `services` (`serviceNO`),
  ADD CONSTRAINT `bookingdetails_ibfk_3` FOREIGN KEY (`productCode`) REFERENCES `products` (`productCode`),
  ADD CONSTRAINT `bookingdetails_ibfk_4` FOREIGN KEY (`staffNO`) REFERENCES `stylists` (`staffNO`);

ALTER TABLE `bookinghead`
  ADD CONSTRAINT `bookinghead_ibfk_1` FOREIGN KEY (`clientNO`) REFERENCES `clients` (`clientNO`);

ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`staffNO`) REFERENCES `stylists` (`staffNO`),
  ADD CONSTRAINT `message_ibfk_2` FOREIGN KEY (`clientNO`) REFERENCES `clients` (`clientNO`),
  ADD CONSTRAINT `message_ibfk_3` FOREIGN KEY (`messageTypeCode`) REFERENCES `messagetypes` (`messageTypeCode`);


ALTER TABLE `operationhours`
  ADD CONSTRAINT `operationHours_ibfk_1` FOREIGN KEY (`branchNO`) REFERENCES `companycontact` (`branchNO`);


ALTER TABLE `prebookingdetails`
  ADD CONSTRAINT `prebookingdetails_ibfk_1` FOREIGN KEY (`preBookingNO`) REFERENCES `prebookinghead` (`preBookingNO`),
  ADD CONSTRAINT `prebookingdetails_ibfk_2` FOREIGN KEY (`serviceNO`) REFERENCES `services` (`serviceNO`),
  ADD CONSTRAINT `prebookingdetails_ibfk_3` FOREIGN KEY (`preferredStylists`) REFERENCES `specialistskills` (`staffNO`),
  ADD CONSTRAINT `prebookingdetails_ibfk_4` FOREIGN KEY (`clientNO`) REFERENCES `prebookinghead` (`clientNO`);


ALTER TABLE `prebookinghead`
  ADD CONSTRAINT `prebookinghead_ibfk_1` FOREIGN KEY (`clientNO`) REFERENCES `clients` (`clientNO`);


ALTER TABLE `specialistskills`
  ADD CONSTRAINT `specialistSkills_ibfk_1` FOREIGN KEY (`serviceNO`) REFERENCES `services` (`serviceNO`);


ALTER TABLE `staffcertications`
  ADD CONSTRAINT `Cert_Invalid` FOREIGN KEY (`certificationCode`) REFERENCES `certifications` (`certificationCode`),
  ADD CONSTRAINT `Styl_Invalid` FOREIGN KEY (`staffNO`) REFERENCES `stylists` (`staffNO`);


ALTER TABLE `staffqualifications`
  ADD CONSTRAINT `qualification_Invalid` FOREIGN KEY (`qualificationCode`) REFERENCES `qualifications` (`qualificationCode`),
  ADD CONSTRAINT `staffqualifications_ibfk_1` FOREIGN KEY (`staffNO`) REFERENCES `stylists` (`staffNO`);


ALTER TABLE `userlogon`
  ADD CONSTRAINT `logonCustomerFK_invalid` FOREIGN KEY (`clientNO`) REFERENCES `clients` (`clientNO`),
  ADD CONSTRAINT `logonUserTypesFK_invalid` FOREIGN KEY (`userTypeCode`) REFERENCES `usertypes` (`userTypeCode`),
  ADD CONSTRAINT `userlogon_ibfk_1` FOREIGN KEY (`staffNO`) REFERENCES `stylists` (`staffNO`);

























