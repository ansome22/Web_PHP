/*
-- Boss Cuts DDL script
-- Created by Sue Beale
-- Created on 4th April 2016 @ 9am


DELETE FROM userLogon;
DELETE FROM stylists;
DELETE FROM client;
DELETE FROM userType;

*/


/*  Clear the screen  
clear screen;
*/


/*  Insert into userType  */
INSERT INTO usertypes(userTypeCode, typeDesc, statusCode) 
VALUES ('S','Staff','E');
INSERT INTO usertypes (userTypeCode, typeDesc, statusCode) 
VALUES ('C','Client','E');
INSERT INTO usertypes (userTypeCode, typeDesc, statusCode) 
VALUES ('A','Administrator','E');

	
/*  Insert into clients  
INSERT INTO clients (lastName, firstName, gender, birthdate, phone, email, streetAddress, suburb, city, postcode)
VALUES ('Beale', 'Sue', 'F', '1980-08-04', 8059, 'Sue.Beale@wintec.ac.nz', '100 Victoria Street', 'Central City', 'Hamilton', '3020');
INSERT INTO clients (lastName, firstName, gender, birthdate, phone, email, streetAddress, suburb, city, postcode)
VALUES ('Jones', 'Bob', 'M',  '1974-21-09', 8030, 'Bob.Jones@wintec.ac.nz', '55 Dinsdale Road', 'Dinsdale', 'Hamilton', '3040');
INSERT INTO clients (lastName, firstName, gender, birthdate, phone, email,  streetAddress, suburb, city, postcode)
VALUES ('Hunter', 'Jody', 'F', '1994-07-08', 0275558989, 'hunterJo@wintec.ac.nz', '5 Dinsdale Road', 'Dinsdale', 'Hamilton', '3040');
INSERT INTO clients (lastName, firstName, gender, birthdate, phone, email,   streetAddress, suburb, city, postcode)
VALUES ('Hunter', 'James', 'M', '2000-08-04', 0275558989, 'hunterJ@wintec.ac.nz', '5 Dinsdale Road', 'Dinsdale', 'Hamilton', '3040');
*/

	
	
/*  Insert into stylists  
INSERT INTO stylists (lastName, firstName, yearsExperience, aboutMe, gender, homeTown, birthdate, phone, email, streetAddress, suburb, city, postcode)
VALUES ('Rakena', 'Blaine', 4, 'I have a passion for hair styling.', 'M', 'Kaikohe', '2000-01-02', 02743214568, 'blainejnr@gmail.com', '4 Fairfield Street', 'Fairfield', 'Hamilton', '3058');
INSERT INTO stylists (lastName, firstName, yearsExperience, aboutMe, gender, homeTown, birthdate, phone, email, streetAddress, suburb, city, postcode)
VALUES ('Rakena', 'Tai', 4, 'I have a passion for making people feel good about themselves.', 'M', 'Kaikohe', '2003-04-28', 0219874563, 'tai@gmail.com', '4 Fairfield Street', 'Fairfield', 'Hamilton', '3058');
INSERT INTO stylists (lastName, firstName, yearsExperience, aboutMe, gender, homeTown, birthdate, phone, email, streetAddress, suburb, city, postcode)
VALUES ('Beatie', 'John', 8, null, 'M', 'Hamilton', '1994-10-11', 027421587432, 'beatie@gmail.com', '10 Marsdan Road', 'Rototuna', 'Hamilton', '3010');
*/

	
/*  Insert into userLogon  */
/* client data 
INSERT INTO userlogon(username, password, clientNO, userTypeCode, statusCode)
VALUES ('suebeale', 'testuser', 1, 'C', 'E');
INSERT INTO userlogon(username, password, clientNO, userTypeCode, statusCode)
VALUES ('bobjones', 'testuser', 2, 'C', 'E');
INSERT INTO userlogon(username, password, clientNO, userTypeCode, statusCode)
VALUES ('hunterJo', 'testuser', 3, 'C', 'E');
INSERT INTO userlogon(username, password, clientNO, userTypeCode, statusCode)
VALUES ('hunterJ', 'testuser', 4, 'C', 'E');
*/

/* stylists ADMIN data 
INSERT INTO userlogon(username, password, staffNO, userTypeCode, statusCode)
VALUES ('blaine', 'testuser', 1, 'A', 'E');
INSERT INTO userlogon(username, password, staffNO, userTypeCode, statusCode)
VALUES ('tai', 'testuser', 1, 'A', 'E');
*/

/* stylists data 
INSERT INTO userlogon(username, password, staffNO, userTypeCode, statusCode)
VALUES ('beatie', 'testuser', 3, 'S', 'E');
*/

/*carried on
*/
INSERT INTO status (statusCode, statusDescription) 
VALUES ('E','Enabled'),
('D','Disabled'),
('L','Locked'),
('P','Pending [app approval]');

/*
INSERT INTO qualifications (qualificationName, qualificationDesc, statusCode)
VALUES ('Hairdressing','Bachelor of Hairdressing at Wintec','E'),
('BarberShip','Certificate in Barbering at Waikato University','E'),
('Hairdressing','Certificate in hairdressing Otago University','E'),
('HealthCare','Certificate in Health Care Victoria University','E');


INSERT INTO staffqualifications (qualificationCode, staffNO, dateObtained, statusCode) VALUES
('1','1','2008-04-05','E'),
('2','2','2009-02-01','P'),
('3','3','2011-05-12','L'),
('3','1','2012-05-05','D');

INSERT INTO certifications (certificationName, certificationDesc, statusCode) VALUES
('HairCert','Certification in HairDressing','E'),
('HairCare','Certification in HairCare','P'),
('StyleCert','Certification in Hair Styling','L');

INSERT INTO staffcertications(certificationCode, staffNO, dateObtained, statusCode) VALUES
('1','1','2009-01-01','E'),
('2','2','2011-12-12','P'),
('3','3','2002-11-6','D'),
('2','1','2014-12-28','E');
*/

INSERT INTO messagetypes(messageTypeCode, typeDescription, status) VALUES
('F','Feedback','E'),
('C','Contact Message','E');
/*

INSERT INTO message(messageTypeCode,staffNO,clientNO,fullName,email,phone,messageDate,subject,messageDescription,
starRating,serviceReceived,replyDescription,replyDate,replyStaff) VALUES
('C','1','2','Sue Beale',NULL,NULL,'2016-04-28','HairDresser','Blaine was the best stylist i have ever had','5','HairCut','Thank you','2016-04-29',1),
('F',NULL,NULL,'Anonymous',NULL,NULL,'2015-04-05','Great Deal','The Deals you guys have are amazing','4',NULL,NULL,NULL,NULL);


INSERT INTO news(newsDate,subject,searchWords,newsText,visible) VALUES
('2016-04-12','Special Deal','deal price hairdressing barber bosscuts kids','Amazing deals, all hair cuts only $10 for kids','Y'),
('2016-02-04','Close Sunday','bosscut holiday sunday close opening hours','This Sunday Boss Cuts will be shut due to family related event','N');
*/

