<!--
                                                        === Services ===
-->
<?php session_start(); ?>

<div class="modal fade" id="Cuts" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Cuts</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered table-hover">
          <thead>
            <tr class="text-white">
              <th>Cut</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            <tr class="orange-text">
              <td>Buzz Cuts and Sides</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>12</td>
            </tr>
            <tr class="orange-text">
              <td>Gents 60+</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>15</td>
            </tr>
            <tr class="orange-text">
              <td>Line up and Taper</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>15</td>
            </tr>
            <tr class="orange-text">
              <td>14yrs and Under</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>15</td>
            </tr>
            <tr class="orange-text">
              <td>High School Students</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>20</td>
            </tr>
            <tr class="orange-text">
              <td>STANDARD Mens Cuts</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>25</td>
            </tr>
          </tbody>
        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="Fades_and_Blended" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Fades and Blended</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered table-hover">
          <thead>
            <tr class="text-white">
              <th>Fades and Blended</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            <tr class="orange-text">
              <td>ZERO fade</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>25.00</td>
            </tr>
            <tr class="orange-text">
              <td>Razor fade</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>30.00</td>
            </tr>
            <tr class="orange-text">
              <td>Restyle (from long to short)</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>30.00</td>
            </tr>
            <tr class="orange-text">
              <td>High Skin fade</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>30.00</td>
            </tr>
          </tbody>
        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="Tracks" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tracks</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered table-hover">
          <thead>
            <tr class="text-white">
              <th>Tracks</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            <tr class="orange-text">
              <td>Hair Art</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>10.00 - 30.00</td>
            </tr>
            <tr class="orange-text">
              <td>Tracks</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>10.00 - 30.00</td>
            </tr>
          </tbody>
        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="Beards" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Beards</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered table-hover">
          <thead>
            <tr class="text-white">
              <th>Beards</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            <tr class="orange-text">
              <td>Line Up</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>5.00 - $10.00</td>
            </tr>
            <tr class="orange-text">
              <td>Trimming</td>
              <td><i class="fa fa-usd" aria-hidden="true"></i>5.00 - $10.00</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!--
                                                                    === Booking ===
-->


<div class="modal fade" id="Bookingmodal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Booking</h4>
      </div>

      <div class="modal-body">
        <!-- Calling javascript validation file for this form -->
        <form id="booking" onSubmit="return validateBooking.js;" method="POST" action=".../Sites/establishContact.php">
          <fieldset class="form-group">
            <label><i class="fa fa-user" aria-hidden="true"></i> Full Name:</label>
            <br>
            <input type="text" name="firstName" id="firstName" placeholder="First Name" style="size:25%">
            <input type="text" name="lastName" id="lastName" placeholder="Last Name" style="size:25%">
            <br>
          </fieldset>
          <fieldset>
            <label>Barber:</label><br>
            <select name="barber" id="barber">
              <option value="James">James</option>
              <option value="Hanna">Hanna</option>
              <option value="Ben">Ben</option>
              <option value="Kerrian">Kerrian</option>
            </select><br>
            <label> Date of Booking: </label><br>
            <input type="date" name="bookingDate" id="bookingDate">
            <br>
            <br>
          </fieldset>
          <fieldset class="form-group">
            <p>You must be registered to make a booking</p>
            <input type="hidden" name="tablename" value="booking">
            <button type="Submit" name="Submit" class="btn btn-success">Submit <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
            <button type="Reset" name="Reset" class="btn btn-danger">Reset <i class="fa fa-repeat" aria-hidden="true"></i></button>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!--
                                                                      === Login ===
-->


<div class="modal fade" id="Login" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-black">Login</h4>
      </div>
      <div class="modal-body">
        <form id="login" action="../Sites/establishContact.php" method="POST" onSubmit="validateLogin.js">
         <fieldset>
          <label>Email:</label><br>
          <input id="email" name="email" placeholder="Email" type="text" class="form-control" required>
          <label>Password:</label><br>
          <input id="password" name="password" placeholder="******" type="text" class="form-control" required><br>
          </fieldset>
            <button type="submit" name="Submit" class="btn btn-success">Submit <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
            <button type="reset" name="Reset" class="btn btn-danger">Reset <i class="fa fa-repeat" aria-hidden="true"></i></button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!--
                                                                           === Feedback ===
-->


<div class="modal fade" id="ModalFeedback" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-black">Feedback</h4>
      </div>
      <div class="modal-body">
      <form id="feedback" onSubmit="return validateFeedback.js" method="POST" action="../Sites/establishContact.php">
        <!-- Form to allow clients to place feedback. Saves to database with hidden page for presentation that administrator level users can access and view feedback through-->
        <fieldset class="form-group">
          <label for="firstName"><i class="fa fa-user" aria-hidden="true"></i> Full Name:</label><br> 
          <input type="text" id="F_firstName" name="F_firstName" placeholder="First Name" size="32" required>
          <input type="text" id="F_lastName" name="F_lastName" placeholder="Last Name" size="32" required>
          <br><label for="email"><i class="fa fa-envelope" aria-hidden="true"></i> Email Address:</label>
          <br><input type="email" class="form-control" id="F_email" placeholder="Example@provider.com" name="F_email">
          <br><label for="phone"><i class="fa fa-phone" aria-hidden="true"></i> Phone Number:</label>
          <br><input type="tel" class="form-control" name="F_phone" id="F_phone" placeholder="(022) 645 65358">
          <br><label> Subject: </label><br><input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
         </fieldset>
          <label><i class="fa fa-star" aria-hidden="true"></i> Rating: </label>
          <fieldset>
             <span class="star-cb-group">
              <input type="radio" id="rating-5" name="rating" value="5" ><label for="rating-5">5</label>
              <input type="radio" id="rating-4" name="rating" value="4" ><label for="rating-4">4</label>
              <input type="radio" id="rating-3" name="rating" value="3" ><label for="rating-3">3</label>
              <input type="radio" id="rating-2" name="rating" value="2" ><label for="rating-2">2</label>
              <input type="radio" id="rating-1" name="rating" value="1" ><label for="rating-1">1</label>
              <input type="radio" id="rating-0" name="rating" value="0" class="star-cb-clear" checked><label for="rating-0">0</label>
            </span>
          </fieldset>
          <fieldset class="form-group">
          <label for="F_message"><i class="fa fa-comment" aria-hidden="true"></i> Message:</label>
          <br><textarea name="F_messageDescription" class="form-control" rows="3" placeholder="Enter message here..." required id= "F_message"></textarea><br>
          <label for="serviceRecieved">Service Received</label>
          <br><input type="text" id="F_serviceRecieved" name="F_serviceRecieved" placeholder="Service Received" style="width:40%">
</fieldset>
        <input type="hidden" id="tablename" name="tablename" value="feedback">
        <input type="hidden" id = "F_messageTypeCode" name="F_messageTypeCode" value="F">
        <button type="Submit" name="Submit" class="btn btn-success">Submit <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
        <button type="Reset" name="Reset" class="btn btn-danger">Reset <i class="fa fa-repeat" aria-hidden="true"></i></button>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!--
                                                                         === Query ===
-->


<div class="modal fade" id="ModalQuery" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-black">Query</h4>
      </div>
      <div class="modal-body">
      <form id="query" onSubmit="return validateQuery.js" method="POST" action="../Sites/establishContact.php"> <!-- YOU HAVE TO ADD Sites/ becasuse its still on index.php (root)-->
        <fieldset>
          <label for="firstName"><i class="fa fa-user" aria-hidden="true"></i> Full Name:</label><br> 
          <input type="text" id="Q_firstName" name="Q_firstName" placeholder="First Name" size="32" required>
          <input type="text" id="Q_lastName" name="Q_lastName" placeholder="Last Name" size="32" required>
         </fieldset>
        <fieldset>
          <label for="email" ><i class="fa fa-envelope" aria-hidden="true"></i> Email Address:</label><br> 
          <input type="email" class="form-control" id="Q_email" placeholder="Example@provider.com" name="Q_email">
          <label for="phone"><i class="fa fa-phone" aria-hidden="true"></i> Phone Number:</label> <br>
          <input type="tel" class="form-control" name="Q_phone" id="Q_phone" placeholder="(022) 645 65358">
          <label for="subject">Subject</label><br>
          <input type="text" name="Q_subject" class="form-control" id="Q_subject" placeholder="Subject of your Message" required>
          <label for="message"><i class="fa fa-comment" aria-hidden="true"></i> Message:</label>
          <br><textarea name="Q_messageDescription" class="form-control" placeholder="Enter message here..." rows="3" required id = "Q_messageDescription"></textarea>
          <input type="hidden" id = "Q_messageTypeCode" name="Q_messageTypeCode" value="Q">
          <input type="hidden" id="tablename" name="tablename" value="query">
        </fieldset>
          <button type="Submit" name="Submit" class="btn btn-success">Submit <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
          <button type="Reset" name="Reset" class="btn btn-danger">Reset <i class="fa fa-repeat" aria-hidden="true"></i></button>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!--
                                                                  === Barbers ===
-->


<div class="modal fade" id="Blaine" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Blaine | Owner Stylist</h4>
      </div>
      <div class="modal-body">
        <p>
          Blaine has used his talent and creativity to make all his clients look their best. Specializing in men’s hair styling exclusively since the idea found him on a life adventure starting in Auckland 2011. In 2012 he joined a hair cutting company in Auckland and soon developed his skills and confidence to pursue ownership of his own Barber Shop and build a successful men’s hairstyling and cutting business. He has put together his ideal Barber Shop to ensure every client receives the best service in the industry.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="Tai" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tai | Owner Stylist</h4>
      </div>
      <div class="modal-body">
        <p>
          Tai began his journey in barbering in 2012. After his brother had completed his training, Tai also went to the same training school with Mr Barber's, where he graduated at the top of his class. He quickly realized his passion for men’s haircuts, styling and grooming. Tai decided to begin his career in Hamilton working for a number of men’s barber shops where he received excellent training and experience in men’s haircuts and styling. Along with his brother Blaine and Tai opened their shop as owner operator in April 2014. 
        </p>
        <p>
          <i class="fa fa-quote-left" aria-hidden="true"></i> My passion is providing the finest service and technique to make sure you Feel The Best! <i class="fa fa-quote-right" aria-hidden="true"></i> -Tai
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="Jimmy" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Jimmy | Stylist</h4>
      </div>
      <div class="modal-body">
        <p>
         Hi my name is Jimmy and I have been Barbering for a year now at Boss Cuts. I chose to specialise in barbering as I love to do modern men’s and boy’s designs but also have a passion for the traditional styles. I am always looking at ways to further my skills and at present I enjoy the art of the cut throat razor and design. 
        </p>
        <p><i class="fa fa-quote-left" aria-hidden="true"></i> I look forward to seeing you soon. <i class="fa fa-quote-right" aria-hidden="true"></i> -Jimmy</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="Siline" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Siline | Stylist</h4>
      </div>
      <div class="modal-body">
        <p>
         I am a barber. I have been in this industry now for 3 years. There are people who argue with me about this. I once had a man who was sitting in my chair getting his hair cut argue that as a woman I couldn’t be a barber. But, well, since I was the one cutting his hair, he was clearly wrong. Men can be hairstylists. Why can’t a woman be a barber? Luckily, in my career I’ve been fortunate to get encouragement more than anything from the men in my field and the men in my chair. I’ve been very fortunate to be given a chance and my own chair in BossCuts. I can do all cuts including tapered skin fades, beard line ups, close cuts, fades and more.
        </p>
        <p><i class="fa fa-quote-left" aria-hidden="true"></i> Come see me soon. <i class="fa fa-quote-right" aria-hidden="true"></i> -Siline</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
