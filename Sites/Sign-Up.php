<html lang="en">

<!-- Head  -->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="author" content="Alex,Adam,Issac,Nick" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="Resource-type" content="Document" />
	<!-- Css   -->
	<link rel="stylesheet" type="text/css" href="../css/jquery.fullpage.css" />
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<!-- Scripts   -->
	<script src="../jquery/jquery-2.2.4.min.js"></script>
	<script src="../jquery/jquery.fullpage.js"></script>
		<script src="../js/tether.min.js"></script>
	<script src="../js/bootstrap.js"></script>
</head>
<!-- Information  -->

<body>
	<!-- Header  -->
	<?php 
		include '../Elements/Header.php';
	?>
<?php include '../Elements/Footer.php';?>
	<!-- Information  -->
	<div class="container">
		

		<!-- PLEASE DON'T TOUCH THE ACTUAL FORM CONTENT :) -->
		<div class="tab-content card-panel">
			<div id="Register" class="tab-pane fade in active">
				<script src="../js/validateRegistration.js"></script>
				<!-- Calling javascript validation file for this form--> 
				<form id="register" onSubmit="action" method="POST" action="addUser.php">
					

					<fieldset>
						<legend>Register New Customer</legend>
						<br>
						<fieldset class="form-group">
						<fieldset class="form-group">
							<!-- Class form-control is setting width to 100%-->
							<label for="Username"></i> Username: </label><br>
							<input type="text" name="username" id="username" placeholder="Enter Username" size="75%" tabIndex="1"> <br>
							<label for="email"><i class="fa fa-envelope" aria-hidden="true"></i> Email address:</label><br>
							<input type="email" name="email" id="email" placeholder="Enter email" size="75%" tabIndex="2" required> <br>
							<!--class="form-control"-->
							<small class="text-muted">We'll never share your email with anyone else.</small>
							<br>
						</fieldset>
							<div class="row">
								<div class="col-md-4">
									<label for="password"><i class="fa fa-key" aria-hidden="true"></i> Password: </label><br>
									<input type="password" name="password" id="Password" placeholder="Password" size="30%" required tabIndex="3">
								</div>
								<div class="col-md-3">
									<label for="repassword"><i class="fa fa-key" aria-hidden="true" required></i> Confirm Password: </label><br>
									<!--class="form-control"--> 
									<input type="password" id="repassword" placeholder="Password" name="repassword" size="30%" required tabIndex="4" required>
									<!--class="form-control"-->
								</div>
							</div>
							<input type="hidden" id="userTypeCode" name="userTypeCode" default="C">
							<input type="hidden" id="statusCode" name="statusCode" default="E">
						</fieldset>
						<fieldset class="form-group">

							<label for="firstName"><i class="fa fa-user" aria-hidden="true"></i> Full Name: </label><br>
							<div class="row">
								<div class="col-md-4">
									<input type="text" name="firstName" size="30%" placeholder="First Name" required tabindex="5" required>
								</div>

								<div class="col-md-3">
									<input type="text" name="lastName" size="30%" placeholder="Last Name" required tabIndex="6" required>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="gender"><i class="fa fa-transgender" aria-hidden="true"></i> Gender: </label><br>
									<select name="gender" id="gender" tabIndex="7">
							<option>Male</option>
							<option>Female</option>
							<option>Other</option>
						</select>
								</div>
								<div class="col-md-3">
								<label for="birthdate"><i class="fa fa-birthday-cake" aria-hidden="true"></i> Date of Birth: </label>
								<input type="date" name="birthdate" id="birthdate" tabindex="8">
								</div>
								<div class="col-md-3">
								<label for="phone"><i class="fa fa-phone" aria-hidden="true"></i> Phone Number: </label><br>
								<input type="tel" name="phone" id="phone" placeholder="(022) 0355 434" tabIndex="9">
								</div>
							</div>
						</fieldset>

						<fieldset class="form-group">
							<label for="Street"><i class="fa fa-map-marker" aria-hidden="true"></i> Street Address:</label><br>
							<input type="text" id="streetAddress" name="streetAddress" size="25%" placeholder="24 Anywhere Lane" tabIndex="10">
							<input type="text" id="suburb" name="suburb" size="15%" placeholder="Suburb" tabIndex="11">
							<input type="text" id="city" name="city" size="15%" placeholder="City" tabIndex="12">
							<input type="text" id="postcode" name="postcode" size="10%" placeholder="Postcode" tabIndex="13">
						</fieldset>
						<br>

						<button type="Submit" name="Submit" class="btn btn-success"> Submit <i class="fa fa-arrow-right" aria-hidden="true" tabIndex="14"></i></button>
						<button type="Reset" name="Reset" class="btn btn-danger"> Reset <i class="fa fa-repeat" aria-hidden="true" tabIndex="15"></i></button>
					</fieldset>
				</form>
				</div>
			</div>
		</div>
</body>