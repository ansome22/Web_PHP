<!--
to do:
 figure out what to do with Copyright (c) year copyright holders for mit licence

-->

<head>
  <!-- Css   -->
  <link rel="stylesheet" type="text/css" href="../css/jquery.fullpage.css" />
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
  <link rel="stylesheet" href="../css/bootstrap.css">
  <link rel="stylesheet" href="../css/blueimp-gallery.min.css">
  <!-- Scripts   -->
  <script src="../jquery/jquery-2.2.4.min.js"></script>
  <script src="../jquery/jquery.fullpage.js"></script>
  <script src="../js/bootstrap.js"></script>
  <script src="../js/tether.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#fullpage').fullpage();
    });
  </script>
</head>

<body>
  <?php require '../Elements/Header.php';?>
  <div id="fullpage">
    <div class="section">
      <div class="slide">
        <h1 class="center-text">Licencing</h1>
        <!--langdon-font-->
        <div class="container">
          <div class="card-panel">
            <article class="center-text">
              <article class="center-text">
                <h3 class="center-text">Langdon Font</h3>
                <p>Langdon Font by xlntelecom - "http://www.fontsquirrel.com/fonts/langdon". </p>
                <p>Langdon Font is Licensed under xlntelecom license:</p>
                <p> xlntelecom</p>
                <p>
                  Introducing Langdon, the new typeface from XLN Telecom. Working in partnership with Steven Bonner - a leading graphic designer, illustrator and typographer based in the UK - we have developed a typeface that is solid, serious and dependable. Langdon is
                  available as a free download and can be used privately and commercially with no restrictions on usage.<br> We hope you like it!

                </p>
              </article>

              </p>
            </article>
          </div>
        </div>
      </div>

      <div class="slide">
        <!--Awesome-font-->
        <div class="container">
          <div class="card-panel">
            <article class="center-text">
              <h3 class="center-text">Font Awesome</h3>
              <p>Font Awesome by Dave Gandy - "http://fontawesome.io". </p>
              <p>Font Awesome code is Licensed under the MIT license:</p>
              <p> The MIT License (MIT)<br> Copyright (c) year copyright holders
                <br><br> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights
                to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
                <br><br>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
                <br><br> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
                OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
              </p>
              <br>
              <p>Font Awesome by Dave Gandy - "http://fontawesome.io". </p>
              <p>Font Awesome Font is Licensed under the SIL Open Font License:</p>
              <p> SIL OFL 1.1</p>

            </article>
          </div>
        </div>
      </div>


    
    <div class="slide">
          <!--fullPage.js-->
    <div class="container">
      <div class="card-panel">
        <article class="center-text">
          <h3 class="center-text">FullPage</h3>
          <p>fullPage.js by alvarotrigo - "https://github.com/alvarotrigo/fullPage.js". </p>
          <p>fullPage.js is Licensed under the MIT license:</p>
          <p> The MIT License (MIT)<br> Copyright (c) year copyright holders
            <br><br> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights
            to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
            <br><br>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
            <br><br> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
            OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
          </p>
        </article>
      </div>
    </div>
    </div>
      
      <div class="slide">
        <!--  Galley  -->
    <div class="container">
      <div class="card-panel">
        <article class="center-text">
          <h3 class="center-text">blueimp Gallery</h3>
          <p>Gallery by blueimp - "https://github.com/blueimp/Gallery". </p>
          <p>Gallery is Licensed under the MIT license:</p>
          <p> The MIT License (MIT)<br> Copyright (c) year copyright holders
            <br><br> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights
            to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
            <br><br>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
            <br><br> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
            OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
          </p>
        </article>
      </div>
    </div>
    </div>
    
          <div class="slide">
        <!--  Galley  -->
    <div class="container">
      <div class="card-panel">
        <article class="center-text">
          <h3 class="center-text">JQuery</h3>
          <p>JQuery by blueimp - "https://github.com/blueimp/Gallery". </p>
          <p>JQuery is Licensed under the MIT license:</p>
          <p> The MIT License (MIT)<br> Copyright (c) year copyright holders
            <br><br> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights
            to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
            <br><br>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
            <br><br> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
            OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
          </p>
        </article>
      </div>
    </div>
    </div>
    
          <div class="slide">
        <!--  Galley  -->
    <div class="container">
      <div class="card-panel">
        <article class="center-text">
          <h3 class="center-text">Bootstrap</h3>
          <p>Bootstrap by blueimp - "https://github.com/blueimp/Gallery". </p>
          <p>Bootstrap is Licensed under the MIT license:</p>
          <p> The MIT License (MIT)<br> Copyright (c) year copyright holders
            <br><br> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights
            to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
            <br><br>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
            <br><br> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
            OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
          </p>
        </article>
      </div>
    </div>
    </div>
    
  </div>
  </div>
</body>