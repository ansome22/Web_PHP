<html lang="en">

<!-- Head  -->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="author" content="Alex,Adam,Issac,Nick" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="Resource-type" content="Document" />
	<!-- Css   -->
	<link rel="stylesheet" type="text/css" href="../css/jquery.fullpage.css" />
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<!-- Scripts   -->
	<script src="../jquery/jquery-2.2.4.min.js"></script>
	<script src="../jquery/jquery.fullpage.js"></script>
	<script src="../js/bootstrap.min.js"></script>
</head>
<!-- Information  -->

<body>
	<!-- Header  -->
	<?php 
		include '../Elements/Header.php';
	?>

	<!-- Information  -->
		<!-- PLEASE DON'T TOUCH THE ACTUAL FORM CONTENT :) -->
		<div class="tab-content card-panel">
			<div id="Register" class="tab-pane fade in active">
				<script src="../js/validateregister.js"></script>
				<!-- Calling javascript validation file for this form--> 
				      <legend>Submit Some Feedback</legend>
      <form id="feedback" onSubmit="return validateFeedback.js" method="POST" action="Sites/establishContact.php">
        <!-- Form to allow clients to place feedback. Saves to database with hidden page for presentation that administrator level users can access and view feedback through-->
        <fieldset class="form-group">
          <label for="firstName"><i class="fa fa-user" aria-hidden="true"></i> Full Name:</label><br> 
          <input type="text" id="F_firstName" name="F_firstName" placeholder="First Name" size="32" required>
          <input type="text" id="F_lastName" name="F_lastName" placeholder="Last Name" size="32" required>
          <br><label for="email"><i class="fa fa-envelope" aria-hidden="true"></i> Email Address:</label>
          <br> <input type="email" id="F_email" placeholder="Example@provider.com" name="F_email" size="32" >
          <br><label for="phone"><i class="fa fa-phone" aria-hidden="true"></i> Phone Number:</label>
          <br><input type="tel" name="F_phone" id="F_phone" placeholder="(022) 645 65358" size="32">
          <br><label> Subject: </label><br><input type="text" id="subject" name="subject" placeholder="Subject" size="32">
         </fieldset>
          <label><i class="fa fa-star" aria-hidden="true"></i> Rating: </label>
          <fieldset>
             <span class="star-cb-group">
              <input type="radio" id="rating-5" name="rating" value="5" ><label for="rating-5">5</label>
              <input type="radio" id="rating-4" name="rating" value="4" ><label for="rating-4">4</label>
              <input type="radio" id="rating-3" name="rating" value="3" ><label for="rating-3">3</label>
              <input type="radio" id="rating-2" name="rating" value="2" ><label for="rating-2">2</label>
              <input type="radio" id="rating-1" name="rating" value="1" ><label for="rating-1">1</label>
              <input type="radio" id="rating-0" name="rating" value="0" class="star-cb-clear" checked><label for="rating-0">0</label>
            </span>
          </fieldset>
          <fieldset class="form-group">
          <label for="F_message"><i class="fa fa-comment" aria-hidden="true"></i> Message:</label>
          <br><textarea name="F_messageDescription" class="form-control" rows="3" placeholder="Enter message here..." required id= "F_message"></textarea><br>
          <label for="serviceRecieved">Service Recieved</label>
          <br><input type="text" id="F_serviceRecieved" name="F_serviceRecieved" placeholder="Service Recieved" style="width:30%">
</fieldset>
        <input type="hidden" id="tablename" name="tablename" value="feedback">
        <input type="hidden" id = "F_messageTypeCode" name="F_messageTypeCode" value="F">
        <button type="Submit" name="Submit" class="btn btn-success">Submit <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
        <button type="Reset" name="Reset" class="btn btn-danger">Reset <i class="fa fa-repeat" aria-hidden="true"></i></button>
      </form>
				</div>
			</div>
</body>