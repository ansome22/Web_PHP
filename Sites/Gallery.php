<div class="container">
  <h1>~ Gallery ~</h1>
</div>
<div class="container hidden-lg-down card-panel">
  <div class="gallery flex-gallery" id="links">
    
    <a href="../Images/Gallery/1.jpg" title="Kid Haircut">
      <img class="grow" src="../Images/Gallery/Small/1.jpg" alt="Kid Haircut">
    </a>

    <a href="../Images/Gallery/2.jpg" title="Outside Of Boss Cuts">
      <img class="grow" src="../Images/Gallery/Small/2.jpg" alt="Outside Of Boss Cuts">
    </a>

    <a href="../Images/Gallery/3.jpg">
      <img class="grow" src="../Images/Gallery/Small/3.jpg" alt="Mans Haircut">
    </a>

    <a href="../Images/Gallery/4.jpg">
      <img class="grow" src="../Images/Gallery/Small/4.jpg" alt="Kid Haircut">
    </a>

    <a href="../Images/Gallery/6.jpg">
      <img class="grow" src="../Images/Gallery/Small/6.jpg" alt="Parking Lot">
    </a>

    <a href="../Images/Gallery/7.jpg">
      <img class="grow" src="../Images/Gallery/Small/7.jpg" alt="Parking Lot">
    </a>

    <a href="../Images/Gallery/8.jpg">
      <img class="grow" src="../Images/Gallery/Small/8.jpg" alt="Parking Lot">
    </a>

    <a href="../Images/Gallery/9.jpg">
      <img class="grow" src="../Images/Gallery/Small/9.jpg" alt="Hair Styling">
    </a>

    <a href="../Images/Gallery/10.jpg">
      <img class="grow" src="../Images/Gallery/Small/10.jpg" alt="Hair Styling">
    </a>

    <a href="../Images/Gallery/11.jpg">
      <img class="grow" src="../Images/Gallery/Small/11.jpg" alt="Trimmers">
    </a>

    <a href="../Images/Gallery/12.jpg">
      <img class="grow" src="../Images/Gallery/Small/12.jpg" alt="Haircuting Tools">
    </a>

    <a href="../Images/Gallery/14.jpg">
      <img class="grow" src="../Images/Gallery/Small/14.jpg" alt title="Haircuting Tools">
    </a>

    <a href="../Images/Gallery/15.jpg">
      <img class="grow" src="../Images/Gallery/Small/15.jpg" alt title="Haircuting Tools">
    </a>


    <a href="../Images/Gallery/16.jpg">
      <img class="grow" src="../Images/Gallery/Small/16.jpg" alt title="Haircuting Tools">
    </a>

    <a href="../Images/Gallery/17.jpg">
      <img class="grow" src="../Images/Gallery/Small/17.jpg" alt title="Haircuting Tools">
    </a>

    <a href="../Images/Gallery/18.jpg">
      <img class="grow" src="../Images/Gallery/Small/18.jpg" alt title="Haircuting Tools">
    </a>

    <a href="../Images/Gallery/19.jpg">
      <img class="grow" src="../Images/Gallery/Small/19.jpg" alt title="Haircuting Tools">
    </a>

    <a href="../Images/Gallery/20.jpg">
      <img class="grow" src="../Images/Gallery/Small/20.jpg" alt title="Haircuting Tools">
    </a>

    <a href="../Images/Gallery/21.jpg">
      <img class="grow" src="../Images/Gallery/Small/21.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    
    <a href="../Images/Gallery/22.jpg">
      <img class="grow" src="../Images/Gallery/Small/22.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    
    <a href="../Images/Gallery/24.jpg">
      <img class="grow" src="../Images/Gallery/Small/24.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    
    <a href="../Images/Gallery/26.jpg">
      <img class="grow" src="../Images/Gallery/Small/26.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    
    <a href="../Images/Gallery/27.jpg">
      <img class="grow" src="../Images/Gallery/Small/27.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    
    <a href="../Images/Gallery/28.jpg">
      <img class="grow" src="../Images/Gallery/Small/28.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    <a href="../Images/Gallery/29.jpg">
      <img class="grow" src="../Images/Gallery/Small/29.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    <a href="../Images/Gallery/31.jpg">
      <img class="grow" src="../Images/Gallery/Small/31.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    <a href="../Images/Gallery/32.jpg">
      <img class="grow" src="../Images/Gallery/Small/32.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    <a href="../Images/Gallery/33.jpg">
      <img class="grow" src="../Images/Gallery/Small/33.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    <a href="../Images/Gallery/34.jpg">
      <img class="grow" src="../Images/Gallery/Small/34.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    <a href="../Images/Gallery/35.jpg">
      <img class="grow" src="../Images/Gallery/Small/35.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    <a href="../Images/Gallery/36.jpg">
      <img class="grow" src="../Images/Gallery/Small/36.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    <a href="../Images/Gallery/37.jpg">
      <img class="grow" src="../Images/Gallery/Small/37.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
    <a href="../Images/Gallery/38.jpg">
      <img class="grow" src="../Images/Gallery/Small/38.jpg" alt title="Haircuting Tools" class="img-fluid">
    </a>
  </div>
</div>

  <!--height:300px width:auto-->
<div class="slideshow hidden-lg-up">
  <div class="gallery-slide">
</div>
 
</div>