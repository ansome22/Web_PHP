<html lang="en">

<!-- Head  -->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="author" content="Alex,Adam,Isaac,Nick" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="Resource-type" content="Document" />
	<!-- Css   -->
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<!-- Scripts   -->
	<script src="../jquery/jquery-2.2.2.min.js"></script>
	<script type="text/javascript" src="../jquery/jquery.fullPage.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#fullpage').fullpage();
			$(".button-collapse").sideNav();
		});
	</script>
</head>
<!-- Information  -->

<body>
	<!-- Header  -->
	<?php include '../Elements/Header.php';
	start_session() ?>
<br><br><br><br><br><br>
	
	<div class="container">
		<div class="Login">
			<img src="../Images/tobelogo.jpg" class="margin-left" width="25%" height="10%">
    <form name = "login" id= "login" method="POST" onSubmit="action" action = "logon.php">
			<input type = "hidden" id = "tablename" name = "tablename" value = "login">
        <div>
          <i class="fa fa-envelope">&nbsp;Username:</i><br>
          <input id="icon_telephone" name="username" placeholder="Username" type="tel" class="validate" required>
        </div>
			
        <div>
          <i class="fa fa-key">&nbsp;Password:</i><br>
          <input placeholder="Password" required id = "password" name = "password">
        </div>
    <input type ="submit"><br><i class="fa fa-sign-in"></i>&nbsp;Login</button>
    </form>
			<a class="" href="#">Lost your password?</a><br>
			<a class="" href="s">Register</a>
		</div>
	</div>
</body>