<!-- Information  -->
<div class="container">
	<h1>~ Our Services ~</h1>
</div>
	<div class="container hidden-lg-down">
		
	<div class="card-panel blur">

		<div class="flex-container">

			<div class="imgpolaroid">
				<img src="../Images/Gallery/16.jpg" alt="Kid Cut" class="responsive-img" style="height:16em;">
				<div class="imgcontainer">
				<button type="button" class="btn btn-primary btn-lg btn-warning" data-toggle="modal" data-target="#Cuts">Cuts</button>
				</div>
			</div>

			<div class="imgpolaroid">
				<img src="../Images/Gallery/18.jpg" alt="Hair Art" class="responsive-img" style="height:16em;">
				<div class="imgcontainer">
					<button type="button" class="btn btn-primary btn-lg btn-warning" data-toggle="modal" data-target="#Tracks">Tracks</button>
				</div>
			</div>

			<div class="imgpolaroid">
				<img src="../Images/Gallery/8.jpg" alt="Kid Cut" class="responsive-img" style="height:16em;">
				<div class="imgcontainer" style="padding-right:20px;">
				<button type="button" class="btn btn-primary btn-lg btn-warning" data-toggle="modal" data-target="#Fades_and_Blended">Fades and<br>Blended</button>
				</div>
			</div>

			<div class="imgpolaroid">
				<img src="../Images/Gallery/22.jpg" alt="Kid Cut" class="responsive-img" style="height:16em;">
				<div class="imgcontainer">
				<button type="button" class="btn btn-primary btn-lg btn-warning" data-toggle="modal" data-target="#Beards">Beards</button>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="container hidden-lg-up">
		
	<div class="card-panel blur">
		<div class="flex-container">
	<a data-toggle="modal" data-target="#Cuts">
				<img src="../Images/Gallery/16.jpg" alt="Kid Cut" class="grow" width="80" height="80">
	</a>
	<a data-toggle="modal" data-target="#Tracks">
				<img src="../Images/Gallery/18.jpg" alt="Hair Art" class="grow" width="80" height="80">
			</a>
	<a data-toggle="modal" data-target="#Fades_and_Blended">
				<img src="../Images/Gallery/8.jpg" alt="Kid Cut" class="grow" width="80" height="80">
		</a>
	<a data-toggle="modal" data-target="#Beards">
				<img src="../Images/Gallery/22.jpg" alt="Kid Cut" class="grow" width="80" height="80">
		</a>
		</div>
		<small class="text-muted">Click to see more information.</small>
	</div>
</div>