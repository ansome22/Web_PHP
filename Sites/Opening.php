<!--linking head doesnt work need to find out why-->
<div class="container">
  <h1>~ Opening Times ~</h1>
  <div class="card-panel blur">
    <div class="flex-container hidden-lg-down">
      <div class="center-text Box-Green z-depth-2 ">
        <h4>Mon:</h4>
        <p class="text-white">9:00 am - 5:00 pm</p>
      </div>
      
      <div class="center-text Box-Green z-depth-2 ">
        <h4>Tue:</h4>
        <p class="text-white">9:00 am - 5:00 pm</p>
      </div>
      
      <div class="center-text Box-Green z-depth-2 ">
        <h4>Wed:</h4>
        <p class="text-white">9:00 am - 5:00 pm</p>
      </div>
      
      <div class="center-text Box-Green z-depth-2">
        <h4>Thu:</h4>
        <p class="text-white">9:00 am - 6:00 pm</p>
      </div>

      <div class="center-text Box-Green z-depth-2">
        <h4>Fri:</h4>
        <p class="text-white">9:00 am - 5:00 pm</p>
      </div>

      <div class="center-text Box-Green z-depth-2">
        <h4>Sat:</h4>
        <p class="text-white">8:30 am - 1:00 am</p>
      </div>
    </div>
    <div class="hidden-lg-up">
      <table class="table table-hover">
        <tr>
          <td>Mon</td>
          <td>9:00 am - 5:00 pm</td>
        </tr>
        <tr>
          <td>Tue:</td>
          <td>9:00 am - 5:00 pm</td>
        </tr>
        <tr>
          <td>Wed:</td>
          <td>9:00 am - 5:00 pm</td>
        </tr>
        <tr>
          <td>Thu:</td>
          <td>9:00 am - 6:00 pm</td>
        </tr>
        <tr>
          <td>Fri:</td>
          <td>9:00 am - 5:00 pm</td>
        </tr>
        <tr>
          <td>Sat:</td>
          <td>8:30 am - 1:00 am</td>
        </tr>
        <tr class="table-danger">
          <td style="color:black;">Sun:</td>
          <td style="color:black;">Closed</td>
        </tr>
      </table>
    </div>
    <button type="button" name="Book" class="z-depth-1 centerbutton btn btn-warning" data-toggle="modal" data-target="#Bookingmodal">Book Now</button>
  </div>
</div>
<br>