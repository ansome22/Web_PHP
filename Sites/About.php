<!--linking head doesnt work need to find out why-->
<div class="container">
  <h1>~ About Us ~</h1>
  <div class="card-panel blur">
    <div id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
        <a data-toggle="collapse" class="orange-text" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          What is Boss Cuts?
        </a>
      </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <p class="text-white">
            When people come into Boss Cuts, I want them to experience a good cut, along with feeling of being welcome, like they are part of the Boss Cuts Family. Boss Cuts is very much in its infancy, with a lot of room to grow. We have a steady stream of returning
            customers and we are always looking for things that we can do better.
          </p>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
        <a class="collapsed orange-text" style="font-size:0.8em" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          How Did Boss Cuts Come To Be?
        </a>
      </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
          <p class="text-white">
            I noticed that there weren’t enough barber shops in Hamilton offering the types of cuts that we wanted. So I thought about having my own barber shop somewhere on the east side of Hamilton and then set off to Auckland to study how to become a Barber. After
            I finished my barber training, I didn’t have the money I needed to open a shop. So I spent time working in barber shops and supplemented that money by doing odd jobs here and there, selling hats, and shoes, and cars so that I could get the money together faster. 
            I did some research and found where I wanted to open a shop, but encountered some difficulty in securing shops. I can only speculate, but I think there was some reluctance
            on behalf of landlords to want to do business with someone young like me but I kept at it, and eventually an offer came up and I was able to secure a shop at our current location at 174 Clarkin Road, Fairfield, in Hamilton.
            <br>- Blaine Rakena<br>Owner of Boss Cuts
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="card-panel flex-container hidden-lg-down">
    <div class="row">
      <div class="col-xs-5 col-md-3">
        <a data-toggle="modal" data-target="#Blaine">
        <picture>
        <source type="image/webp" srcset="../Images/Barber/blaine.webp">
        <img class="img-fluid circle grow" alt="Blaine" src="../Images/Barber/blaine.jpg">
      </picture>
      
        </a>
      </div>
      <div class="col-xs-5 col-md-3">
      <a data-toggle="modal" data-target="#Jimmy">
      <picture>
        <source type="image/webp" srcset="../Images/Barber/Jimmy.webp">
        <img class="img-fluid circle grow" alt="Blaine" src="../Images/Barber/Jimmy.jpg">
      </picture>
      </a>
      </div>
      <div class="col-xs-5 col-md-3">
      <a data-toggle="modal" data-target="#Siline">
      <picture>
        <source type="image/webp" srcset="../Images/Barber/siline.webp">
        <img class="img-fluid circle grow" alt="Blaine" src="../Images/Barber/siline.jpg">
      </picture>
      </a>
      </div>
      <div class="col-xs-5 col-md-3">
      <a data-toggle="modal" data-target="#Tai">
      <picture>
        <source type="image/webp" srcset="../Images/Barber/tai.webp">
        <img class="img-fluid circle grow" alt="Blaine" src="../Images/Barber/tai.jpg">
      </picture>
      </a>
      </div>
    </div>
  </div>
</div>
<br>