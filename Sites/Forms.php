<!--to warn you, you have multible of the same ids which will use the first id that is found-->

<div style="margin-bottom:4.4em;" class="container">
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Query">Query</a></li>
  </ul>
<!--Adjust form ids and names to be more unique-->
  <div class="tab-content card-panel">
    <div id="Query" class="tab-pane fade in active">
      <!-- Calling javascript validation file for this form - Form for the submitting of queries or requests from clients-->
      <legend>Send us a Query</legend>
      <form id="query" onSubmit="return validateQuery.js" method="POST" action="Sites/establishContact.php"> <!-- YOU HAVE TO ADD Sites/ becasuse its still on index.php (root)-->
        <fieldset>
          <label for="firstName"><i class="fa fa-user" aria-hidden="true"></i> Full Name:</label><br> 
          <input type="text" id="Q_firstName" name="Q_firstName" placeholder="First Name" size="32" required>
          <input type="text" id="Q_lastName" name="Q_lastName" placeholder="Last Name" size="32" required>
         </fieldset>
        <fieldset>
          <label for="email" ><i class="fa fa-envelope" aria-hidden="true"></i> Email Address:</label><br> 
          <input type="email" class="form-control" id="Q_email" placeholder="Example@provider.com" name="Q_email">
          <label for="phone"><i class="fa fa-phone" aria-hidden="true"></i> Phone Number:</label> <br>
          <input type="tel" class="form-control" name="Q_phone" id="Q_phone" placeholder="(022) 645 65358">
          <label for="subject">Subject</label><br>
          <input type="text" name="Q_subject" class="form-control" id="Q_subject" placeholder="Subject of your Message" required>
          <label for="message"><i class="fa fa-comment" aria-hidden="true"></i> Message:</label>
          <br><textarea name="Q_messageDescription" class="form-control" placeholder="Enter message here..." rows="3" required id = "Q_messageDescription"></textarea><br>
          <br>
          <input type="hidden" id = "Q_messageTypeCode" name="Q_messageTypeCode" value="Q">
          <input type="hidden" id="tablename" name="tablename" value="query">
        </fieldset>
          <button type="Submit" name="Submit" class="btn btn-success">Submit <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
          <button type="Reset" name="Reset" class="btn btn-danger">Reset <i class="fa fa-repeat" aria-hidden="true"></i></button>
      </form>
    </div>
  </div>
</div>
<br><br>